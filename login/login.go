package login

import (
	"bytes"
	"crypto/rsa"
	"encoding/binary"
	"fmt"
	"io"
	"net"

	tnet "badc0de.net/pkg/go-tibia/net"

	"github.com/golang/glog"
)

type LoginServer struct {
	pk *rsa.PrivateKey
}

// NewServer creates a new LoginServer which can decrypt the initial login message using the passed RSA private key.
func NewServer(pk *rsa.PrivateKey) (*LoginServer, error) {
	return &LoginServer{
		pk: pk,
	}, nil
}

// Serve begins serving the login protocol on the accepted network connection.
//
// User of this method needs to bring their own listening schema and accept the connection,
// then pass on the control to this method.
func (c *LoginServer) Serve(conn net.Conn, initialMessage *tnet.Message) error {
	defer conn.Close()

	msg := initialMessage

	r := io.LimitReader(msg, 2 /* os */ +2 /* version */ +4*3 /* dat, spr, pic sigs */)

	var connHeader struct {
		OS, Version            uint16
		DatSig, SprSig, PicSig uint32
	}

	err := binary.Read(r, binary.LittleEndian, &connHeader)
	if err != nil {
		return fmt.Errorf("could not read conn header: %s", err)
	}

	glog.V(2).Infof("header: %+v", connHeader)
	err = msg.RSADecryptRemainder(c.pk)
	if err != nil {
		return fmt.Errorf("rsa decrypt remainder error: %s", err)
	}

	var keys struct {
		Version byte // is this name right?
		Keys    [4]uint32
	}
	r = io.LimitReader(msg, 1+4*4)
	err = binary.Read(r, binary.LittleEndian, &keys)
	if err != nil {
		return fmt.Errorf("key read error: %s", err)
	}

	// XTEA in Go is bigendian-only. It treats the key as a single
	// 128-bit integer, stored as bigendian. It then explodes it
	// into [4]uint32.
	//
	// We need to flip the order of bytes in the key, otherwise
	// we would quite easily be able to use Keys [16]byte and be
	// done with it.
	key := [16]byte{}
	keyB := &bytes.Buffer{}
	err = binary.Write(keyB, binary.BigEndian, keys.Keys)
	if err != nil {
		return fmt.Errorf("could not convert binary order of keys: %s", err)
	}
	for i := range key {
		key[i] = keyB.Bytes()[i]
	}

	acc, err := msg.ReadTibiaString()
	if err != nil {
		return fmt.Errorf("account read error: %s", err)
	}

	pwd, err := msg.ReadTibiaString()
	if err != nil {
		return fmt.Errorf("pwd read error: %s", err)
	}

	// skip hw spec
	msg.Next(47)

	glog.Infof("acc:%s len(pwd):%d\n", acc, len(pwd))

	////////

	resp := tnet.NewMessage()
	err = MOTD(resp, "1\nHello!")
	if err != nil {
		glog.Errorln("error generating the motd message: ", err)
		return err
	}
	/*
		// add checksum and size headers wherever appropriate, and perform
		// XTEA crypto.
		resp, err = resp.Finalize(key)
		if err != nil {
			glog.Errorf("error finalizing login message response: %s", err)
			return err
		}

		// transmit the response
		wr, err := io.Copy(conn, resp)
		if err != nil {
			glog.Errorf("error writing login message response: %s", err)
			return err
		}
		glog.V(2).Infof("written %d bytes", wr)
	*/
	///////
	localAddr := conn.LocalAddr()
	if localAddr == nil {
		glog.Errorln("could not get local addr")
		return fmt.Errorf("error getting local addr")
	}
	glog.Infof("connection accepted via %v", localAddr)
	localTCPAddr := localAddr.(*net.TCPAddr)
	if localTCPAddr == nil {
		glog.Errorln("could not get local TCP addr")
		return fmt.Errorf("error getting local TCP addr")
	}

	//	resp = tnet.NewMessage()
	err = CharacterList(resp, []CharacterListEntry{
		{
			CharacterName:  "Demo Character",
			CharacterWorld: "Demo World",
			GameFrontend: net.TCPAddr{
				IP:   localTCPAddr.IP,
				Port: 7172,
			},
		},
	}, 30)
	if err != nil {
		glog.Errorln("error generating the character list message: ", err)
		return err
	}

	// add checksum and size headers wherever appropriate, and perform
	// XTEA crypto.
	resp, err = resp.Finalize(key)
	if err != nil {
		glog.Errorf("error finalizing login message response: %s", err)
		return err
	}

	// transmit the response
	wr, err := io.Copy(conn, resp)
	if err != nil {
		glog.Errorf("error writing login message response: %s", err)
		return err
	}
	glog.V(2).Infof("written %d bytes", wr)

	//////////

	return nil
}

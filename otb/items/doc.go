// Package itemsotb reads in an items.otb file.
//
// Top level OTB node's data represents version header, and its children
// represents individual items.
package itemsotb

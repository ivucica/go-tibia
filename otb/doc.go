// Package otb reads in the 'OpenTibia Binary' format.
//
// This format is used in items.otb specifying item attributes, as well as a
// mapping from a client ID to a persistent server ID.
//
// It is also used in the OpenTibia Binary Map with extension .otbm.
package otb

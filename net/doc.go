// Package net implements network communication primitives for the login and gameworld protocol.
//
// This includes a message (a single communications block sent by client or server)
// and encryption primitives.
package net

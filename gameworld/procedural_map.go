package gameworld

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"math/rand"
	"sync"

	"badc0de.net/pkg/go-tibia/dat"
	tnet "badc0de.net/pkg/go-tibia/net"
	"badc0de.net/pkg/go-tibia/things"

	"github.com/golang/glog"
)

// NewMapDataSource returns a new procedural map data source, good for testing.
func NewMapDataSource() MapDataSource {
	return &mapDataSource{
		creatures:         map[CreatureID]Creature{},
		generatedMapTiles: map[tnet.Position]MapTile{},
		mapTileGenerator:  generateMapTileImpl,
	}
}

//////////////////////////////

func BakeTestOnlyCreature(id CreatureID, pos tnet.Position, dir things.CreatureDirection, look uint16, col [4]things.OutfitColor) Creature {
	return &creature{
		pos: pos, id: id, dir: dir, look: look, col: col,
	}
}

type creature struct {
	pos tnet.Position
	id  CreatureID
	dir things.CreatureDirection

	look uint16
	col  [4]things.OutfitColor
}

func (c *creature) GetPos() tnet.Position {
	return c.pos
}
func (c *creature) GetID() CreatureID {
	return c.id
}
func (c *creature) GetName() string {
	return "Demo Character"
}
func (c *creature) GetServerType() uint16 {
	return c.look
}
func (c *creature) GetOutfitColors() [4]things.OutfitColor {
	return c.col
}

func (c *creature) SetPos(p tnet.Position) error {
	c.pos = p
	return nil
}

func (c *creature) GetDir() things.CreatureDirection {
	return c.dir
}

func (c *creature) SetDir(dir things.CreatureDirection) error {
	c.dir = dir
	return nil
}

////////////////////////////

type mapDataSource struct {
	creatures map[CreatureID]Creature

	generatedMapTiles     map[tnet.Position]MapTile
	generatedMapTilesLock sync.Mutex

	mapTileGenerator func(x, y uint16, z uint8) (MapTile, error)
}
type mapTile struct {
	ground    MapItem
	creatures []Creature

	subscribers []MapTileEventSubscriber
}

func (*mapTile) String() string {
	return "<procedural map tile>"
}

type mapItem int

func mapItemOfType(t int) MapItem {
	mi := mapItem(t)
	return &mi
}

const (
	//nightAmbient = color.RGBA{0, 0, 20, 240}
	//nightAmbient = color.RGBA{20, 20, 40, 240}
	NightAmbient      = dat.DatasetColor(0xD7)
	NightAmbientLevel = uint8(40)
	DayAmbient        = dat.DatasetColor(0xD7)
	DayAmbientLevel   = uint8(250)
)

///////////////////////////

func (ds *mapDataSource) Private_And_Temp__DefaultPlayerSpawnPoint(c CreatureID) tnet.Position {
	return tnet.Position{
		X:     uint16(32768 + 18/2 + int(c)),
		Y:     32768 + 14/2,
		Floor: 7,
	}
}

func (ds *mapDataSource) GetAmbientLight() (dat.DatasetColor, uint8) {
	return NightAmbient, NightAmbientLevel
}

func (ds *mapDataSource) GetMapTile(x, y uint16, z uint8) (MapTile, error) {
	ds.generatedMapTilesLock.Lock()
	defer ds.generatedMapTilesLock.Unlock()

	if t, ok := ds.generatedMapTiles[tnet.Position{x, y, z}]; ok {
		return t, nil
	}
	generatedMapTile, err := ds.mapTileGenerator(x, y, z)
	if err != nil {
		return nil, err
	}

	ds.generatedMapTiles[tnet.Position{x, y, z}] = generatedMapTile

	if x == 32768+5 && y == 32768+5 && z == 7 {
		cr := &creature{id: CreatureID(1234 | CreatureTypePlayer), pos: tnet.Position{X: x, Y: y, Floor: z}, look: 128, col: [4]things.OutfitColor{
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
		}}
		ds.AddCreature(cr)
	}
	if x == 109 && y == 89 && z == 7 {
		cr := &creature{id: CreatureID(1235 | CreatureTypeMonster), pos: tnet.Position{X: x, Y: y, Floor: z}, dir: things.CreatureDirectionNorth, look: 128, col: [4]things.OutfitColor{
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
		}}
		ds.AddCreature(cr)
	}
	if x == 111 && y == 89 && z == 7 {
		cr := &creature{id: CreatureID(1236 | CreatureTypeNPC), pos: tnet.Position{X: x, Y: y, Floor: z}, dir: things.CreatureDirectionWest, look: 128, col: [4]things.OutfitColor{
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
			things.OutfitColor(rand.Int() % things.OutfitColorCount()),
		}}
		ds.AddCreature(cr)
	}

	return generatedMapTile, nil

}
func generateMapTileImpl(x, y uint16, z uint8) (MapTile, error) {
	switch z {
	default:
		return &mapTile{}, nil
	case 7:
		if y == 32768+14/2 {
			switch x % 2 {
			case 0:
				return &mapTile{ground: mapItemOfType(104)}, nil
			case 1:
				return &mapTile{ground: mapItemOfType(103)}, nil
			}
		}
		switch ((y + 3) / 2) % 6 {
		case 0:
			return &mapTile{ground: mapItemOfType(103)}, nil
		case 1:
			return &mapTile{ground: mapItemOfType(104)}, nil
		case 2:
			return &mapTile{ground: mapItemOfType(101)}, nil
		case 3:
			return &mapTile{ground: mapItemOfType(100)}, nil
		case 4:
			return &mapTile{ground: mapItemOfType(106)}, nil
		case 5:
			return &mapTile{ground: mapItemOfType(405)}, nil
		default:
			return &mapTile{}, nil
		}
	case 6:
		if x == 32768+(18/2)-4 && y == 32768+(14/2) {
			glog.Infof("sending 104 at %d %d %d", x, y, z)
			return &mapTile{ground: mapItemOfType(103)}, nil
		}
		if x > 90 && x < 97 && y > 91 && y < 97 {
			// for unit test of mapInitialAppear
			return &mapTile{ground: mapItemOfType(405)}, nil
		}

		return &mapTile{}, nil
	}
}
func (ds *mapDataSource) GetCreatureByIDBytes(idBytes [4]byte) (Creature, error) {
	buf := bytes.NewBuffer(idBytes[:])
	var id CreatureID
	err := binary.Read(buf, binary.LittleEndian, &id)
	if err != nil {
		return nil, fmt.Errorf("could not decode creature ID from bytes: %v", err)
	}

	return ds.GetCreatureByID(id)
}
func (ds *mapDataSource) GetCreatureByID(id CreatureID) (Creature, error) {
	if creature, ok := ds.creatures[id]; ok {
		return creature, nil
	}
	return nil, CreatureNotFound
}
func (ds *mapDataSource) AddCreature(c Creature) error {
	ds.creatures[c.GetID()] = c
	if t, err := ds.GetMapTile(c.GetPos().X, c.GetPos().Y, c.GetPos().Floor); err != nil {
		return err
	} else {
		glog.Infof("adding creature to %d %d %d", c.GetPos().X, c.GetPos().Y, c.GetPos().Floor)
		return t.AddCreature(c)
	}
}
func (ds *mapDataSource) RemoveCreatureByID(id CreatureID) error {
	c, err := ds.GetCreatureByID(id)
	if err != nil {
		if err == CreatureNotFound {
			return nil
		}
	}

	delete(ds.creatures, id)

	if t, err := ds.GetMapTile(c.GetPos().X, c.GetPos().Y, c.GetPos().Floor); err != nil {
		return err
	} else {
		glog.Infof("deleting creature from %d %d %d", c.GetPos().X, c.GetPos().Y, c.GetPos().Floor)
		return t.RemoveCreature(c)
	}
}

func (t *mapTile) GetCreature(idx int) (Creature, error) {
	if idx >= len(t.creatures) {
		if idx > len(t.creatures) {
			glog.Infof("creature not found; requested idx %d with len %d", idx, len(t.creatures))
		}
		return nil, CreatureNotFound
	}
	return t.creatures[idx], nil
}
func (t *mapTile) GetItem(idx int) (MapItem, error) {
	if idx == 0 && t.ground != nil && t.ground.GetServerType() != 0 {
		return t.ground, nil
	}
	return nil, ItemNotFound
}
func (t *mapTile) AddCreature(c Creature) error {
	t.creatures = append(t.creatures, c)
	return nil
}

func (t *mapTile) RemoveCreature(cr Creature) error {
	// not t.creatures - 1, in case the creature is not in fact stored on the tile.
	newCs := make([]Creature, 0, len(t.creatures))
	seen := false
	for _, c := range t.creatures {
		if c.GetID() == cr.GetID() {
			seen = true
		} else {
			newCs = append(newCs, c)
		}
	}
	if !seen {
		glog.Warningf("removing creature %d from tile %d %d %d where it's actually not present", cr.GetID(), cr.GetPos().X, cr.GetPos().Y, cr.GetPos().Floor)
	}
	t.creatures = newCs
	return nil
}

// GetServerType returns the server-side ID of the item.
func (i *mapItem) GetServerType() uint16 {
	return uint16(*i)
}

// GetCount returns the number of items in this stackable item.
//
// (This may also be zero in other implementations for nonstackable items.)
func (i *mapItem) GetCount() uint16 {
	return 1
}

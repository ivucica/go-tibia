module badc0de.net/pkg/go-tibia

go 1.11

require (
	badc0de.net/pkg/flagutil v1.0.1
	github.com/BourgeoisBear/rasterm v1.0.3
	github.com/SherClockHolmes/webpush-go v1.2.0
	github.com/andybons/gogif v0.0.0-20140526152223-16d573594812
	github.com/bradfitz/iter v0.0.0-20191230175014-e8f45d346db8
	github.com/common-nighthawk/go-figure v0.0.0-20210622060536-734e95fb86be
	github.com/ericpauley/go-quantize v0.0.0-20200331213906-ae555eb2afa4
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/gookit/color v1.2.3
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.7.2
	github.com/mattn/gowasmer v0.0.0-20220518070401-e6bdba3bec84
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.9.1
	github.com/vedhavyas/go-wasm v0.0.0-20200730123001-8b546c207480
	github.com/vincent-petithory/dataurl v1.0.0
	github.com/wasmerio/wasmer-go v1.0.4
	golang.org/x/crypto v0.0.0-20220112180741-5e0467b6c7ce
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
)
